# KBAS

This is a reimplementation of kbas in Haskell and purescript

Do this to make it work

    mkdir uploads
    stack build
    stack exec -- kbas config.yaml
    curl -X POST --form file=@file.yaml http://localhost:3000/upload

To make the frontend work

    bower install
    pulp browserify --to public/main.js
    chromium public/index.html
