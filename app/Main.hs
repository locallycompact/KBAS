{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module Main where

import           Control.Foldl as Fold hiding (fold, mconcat)
import           Control.Monad.IO.Class
import           Control.Monad.Reader
import qualified Data.ByteString.Char8 as BS
import qualified Data.ByteString.Lazy as B
import           Data.List
import           Data.Maybe
import           Data.Monoid
import           Data.Profunctor
import           Data.Yaml
import           GHC.Generics
import qualified Filesystem.Path.CurrentOS as FS
import           Network.HTTP.Types (status200)
import           Network.Wai.Middleware.Cors
import           Network.Wai.Middleware.RequestLogger
import           Network.Wai.Middleware.Static
import           Network.Wai.Parse
import           Prelude hiding (FilePath)
import           System.Console.Docopt
import           System.Environment (getArgs)
import           System.FilePath ((</>))
import           System.Exit
import           Turtle hiding (die, date, stripPrefix, (</>))
import           Web.Scotty

patterns = [docoptFile|USAGE.txt|]

getArgOrExit = getArgOrExitWith patterns

onString = dimap FS.encodeString FS.decodeString

rollup = flip fold Fold.list

data Config = Config { port :: Int, uploads :: String }
  deriving (Show, FromJSON, Generic)

data Artifact = Artifact { idx :: String, date :: UTCTime }
  deriving (Show, FromJSON, ToJSON, Generic)

type ArtifactStore = ReaderT Config Shell

stripUploadDir :: String -> FilePath -> FilePath
stripUploadDir x = onString $ fromJust . stripPrefix (x <> "/")

listUploads :: ArtifactStore FilePath
listUploads = do
  updir <- asks uploads
  lift . liftM2 (<$>) stripUploadDir (lstree . FS.decodeString) $ updir

getArtifact :: FilePath -> ArtifactStore Artifact
getArtifact x = do
   updir <- asks uploads
   date <- datefile . FS.decodeString . (</> FS.encodeString x) $ updir
   lift . return $ Artifact (FS.encodeString x) date

main = do
  args  <- parseArgsOrExit patterns =<< getArgs
  cfile <- args `getArgOrExit` argument "config"
  yaml  <- BS.readFile cfile
  let c = decode yaml :: Maybe Config
  case c of
    Nothing -> die "Could not load config, exiting."
    Just x -> print x 
  let conf = fromJust c
  scotty (port conf) $ do
    middleware logStdoutDev
    middleware simpleCors
    middleware $ staticPolicy (noDots >-> addBase (uploads conf))

    get "/search/:regex" $ do
      beam <- param "regex"
      gs   <- rollup $ FS.encodeString <$> runReaderT listUploads conf
      let hs = filter (isInfixOf beam) gs
      js <- rollup $ flip runReaderT conf $ mapM getArtifact $ FS.decodeString <$> hs
      json $ mconcat js

    get "/get/:file" $ do
      beam <- param "file"
      setHeader "Content-Type" "application/x-tar"
      file $ uploads conf </> beam

    post "/upload" $ do
      fs <- files
      liftIO . print $ fs
      let fs' = [ (fieldName, BS.unpack (fileName fi), fileContent fi) | (fieldName,fi) <- fs ]
      liftIO . sequence_ $ [ B.writeFile (uploads conf </> fn) fc | (_,fn,fc) <- fs' ]
