module Main where

import Prelude hiding (id)
import Control.Monad.Aff
import Control.Monad.Eff
import Data.Argonaut
import Data.Either
import Data.Maybe
import Data.Traversable
import Network.HTTP.Affjax
import Pux (start, CoreEffects, EffModel, noEffects)
import Pux.DOM.HTML
import Pux.DOM.HTML.Attributes
import Pux.DOM.Events
import Pux.Renderer.React 
import Text.Smolder.HTML (a, div, h1, input, map, table, td, tr) 
import Text.Smolder.HTML.Attributes
import Text.Smolder.Markup

main :: Eff (CoreEffects (ajax :: AJAX)) Unit
main = do
  app <- start
    { initialState: init
    , foldp: update
    , view: view
    , inputs: [] }

  renderToDOM "#app" app.markup app.input

data Action = SearchUpdated DOMEvent
            | RequestArtifacts String
            | ReceiveArtifacts (Either String Artifacts)

type State =
 { artifacts :: Artifacts
  , status :: String
  , search :: String}

type Artifacts = Array Artifact

newtype Artifact = Artifact
  { id :: String,
    datetime :: String }

init :: State
init = { artifacts: [], status: "Nothing loaded from server yet", search : ""}

instance decodeJsonArtifact :: DecodeJson Artifact where
  decodeJson json = do
    obj <- decodeJson json
    id <- obj .? "idx"
    datetime <- obj .? "date"
    pure $ Artifact { id: id, datetime: datetime }

update :: Action -> State -> EffModel State Action (ajax :: AJAX)
update (ReceiveArtifacts (Left err)) state =
  noEffects $ state { status = "Error fetching artifacts: " <> show err }
update (ReceiveArtifacts (Right artifacts)) state =
  noEffects $ state { artifacts = artifacts, status = "Artifacts" }
update (SearchUpdated ev) state =
  { state : state { search = targetValue ev }
  , effects: [ pure $ Just $ RequestArtifacts $ targetValue ev ]
  }
update (RequestArtifacts s) state =
  { state: state { status = "Fetching artifacts..." }
  , effects: [ do
      res <- attempt $ get $ "http://localhost:3000/search/" <> s
      let decode r = decodeJson r.response :: Either String Artifacts
      let artifacts = either (Left <<< show) decode res
      pure $ Just $ ReceiveArtifacts artifacts
    ]
  }

view :: State -> HTML Action
view state =
  div do
    h1 $ text state.status
    div do
      input ! type' "text" ! value state.search #! onChange SearchUpdated
      table ! id "tab" $ do
        for_ state.artifacts artifact

artifact :: Artifact -> HTML Action
artifact (Artifact state) =
  tr ! className "artifact" $ do
    td $ text state.datetime
    td $ a ! href ("http://localhost:3000/get/" <> state.id) $ text state.id
